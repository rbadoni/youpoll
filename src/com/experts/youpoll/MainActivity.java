package com.experts.youpoll;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.app.Activity;
import android.app.LoaderManager;
import android.content.Context;
import android.content.Loader;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;

import com.experts.command.Categories;
import com.experts.command.Question;
import com.experts.util.AsyncCategoryLoader;
import com.experts.util.AsyncQuestionLoader;

public class MainActivity extends Activity {

	ExpandableListAdapter listAdapter;
	ExpandableListView expListView;
	List<String> listDataHeader;
	HashMap<String, List<String>> listDataChild;
	Question[] question;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		// get the listview
		expListView = (ExpandableListView) findViewById(R.id.lvExp);

		// preparing list data
		prepareListData();

		// setting list adapter

		getLoaderManager().initLoader(0, savedInstanceState,
				new LoaderManager.LoaderCallbacks<Question[]>() {
					@Override
					public Loader<Question[]> onCreateLoader(int id, Bundle args) {
						return new AsyncQuestionLoader(MainActivity.this);
					}

					@Override
					public void onLoadFinished(Loader<Question[]> loader,
							Question[] data) {
						// blogListAdapter.setData(data);
						question = data;
						System.out.println("List loaded.....");
						prepareListData(data);
						listAdapter = new ExpandableListAdapter(
								MainActivity.this, listDataHeader,
								listDataChild);
						expListView.setAdapter(listAdapter);
					}

					@Override
					public void onLoaderReset(Loader<Question[]> loader) {
						// blogListAdapter.setData(new ArrayList<String>());
					}
				}).forceLoad();

		getLoaderManager().initLoader(1, savedInstanceState,
				new LoaderManager.LoaderCallbacks<Categories[]>() {
					@Override
					public Loader<Categories[]> onCreateLoader(int id,
							Bundle args) {
						return new AsyncCategoryLoader(MainActivity.this);
					}

					@Override
					public void onLoadFinished(Loader<Categories[]> loader,
							Categories[] categories) {
						// blogListAdapter.setData(data);
						// question = data;
						System.out.println("Category loaded.....");
						LinearLayout lineaLayout= (LinearLayout)findViewById(R.id.categoryContainer);
						for (int i = 0; i < categories[0].getCategories().size(); i++) {
							LayoutInflater infalInflater = (LayoutInflater) MainActivity.this
									.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
							Button button = (Button) infalInflater.inflate(
									R.layout.category_button, null);
							button.setText(categories[0].getCategories().get(i));
							lineaLayout.addView(button);
						}						
						// prepareListData(data);

					}

					@Override
					public void onLoaderReset(Loader<Categories[]> loader) {
						// blogListAdapter.setData(new ArrayList<String>());
					}
				}).forceLoad();
	}

	private void prepareListData() {
		listDataHeader = new ArrayList<String>();
		listDataChild = new HashMap<String, List<String>>();
	}

	/*
	 * Preparing the list data
	 */

	private void prepareListData(Question[] question) {
		// listDataHeader = new ArrayList<String>();
		// listDataHeader.
		// listDataChild = new HashMap<String, List<String>>();

		if (question != null) {
			for (int i = 0; i < question.length; i++) {
				listDataHeader.add(question[i].getQuestionStatement());
				List<String> temp = new ArrayList<String>();
				for (int j = 0; j < question[i].getAnswers().size(); j++) {
					temp.add(question[i].getAnswers().get(j)
							.getAnswerStatement());
				}
				listDataChild.put(question[i].getQuestionStatement(), temp);
			}
		}

		// Adding child data
		/*
		 * listDataHeader.add("The Best cricketer");
		 * listDataHeader.add("Best Leader");
		 * listDataHeader.add("Best Cricket Team");
		 * 
		 * // Adding child data List<String> top250 = new ArrayList<String>();
		 * top250.add("Sachin"); top250.add("Lara"); top250.add("Gavaskar");
		 * 
		 * List<String> nowShowing = new ArrayList<String>();
		 * nowShowing.add("Modi"); nowShowing.add("Kejriwal");
		 * nowShowing.add("Manmohal");
		 * 
		 * List<String> comingSoon = new ArrayList<String>();
		 * comingSoon.add("India"); comingSoon.add("South Africa");
		 * comingSoon.add("Pakistan");
		 * 
		 * listDataChild.put(listDataHeader.get(0), top250); // Header, Child
		 * data listDataChild.put(listDataHeader.get(1), nowShowing);
		 * listDataChild.put(listDataHeader.get(2), comingSoon);
		 */
	}
}