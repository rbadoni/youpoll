package com.experts.util;

public class StringUtils
{
	public static String getStr(String str)
	{
		return str == null ? "" : str.trim();
	}

	public static String notNull(String str)
	{
		return str == null ? "" : str.trim();
	}

	public static String addSpace(String string)
	{
		StringBuilder result = new StringBuilder();

		for (int i = 0; i < string.length(); i++)
		{
			result = result.append(string.charAt(i));
			if (i == string.length() - 1)
				break;
			result = result.append(' ');
		}
		return (result.toString());
	}

	public static boolean isEmpty(String str)
	{
		return (str != null && "".equals(str));
	}

	public static boolean isNotNull(String value)
	{
		if (value == null || value.trim().length() == 0)
		{
			return false;
		}
		else
			return true;
	}	
}
