package com.experts.command;

import java.util.Date;

public class Answers {
	private String _id;
	private String lastLikeTime;
	private int likes;
	private int answerNumber;
	private String answerStatement;
	public String get_id() {
		return _id;
	}
	public void set_id(String _id) {
		this._id = _id;
	}
	public String getLastLikeTime() {
		return lastLikeTime;
	}
	public void setLastLikeTime(String lastLikeTime) {
		this.lastLikeTime = lastLikeTime;
	}
	public int getLikes() {
		return likes;
	}
	public void setLikes(int likes) {
		this.likes = likes;
	}
	public int getAnswerNumber() {
		return answerNumber;
	}
	public void setAnswerNumber(int answerNumber) {
		this.answerNumber = answerNumber;
	}
	public String getAnswerStatement() {
		return answerStatement;
	}
	public void setAnswerStatement(String answerStatement) {
		this.answerStatement = answerStatement;
	}
	
	
}