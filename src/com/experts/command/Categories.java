package com.experts.command;

import java.util.ArrayList;
import java.util.List;

public class Categories {

	private String _id;
	private List<String> categories = new ArrayList<String>();
	public String get_id() {
		return _id;
	}
	public void set_id(String _id) {
		this._id = _id;
	}
	public List<String> getCategories() {
		return categories;
	}
	public void setCategories(List<String> categories) {
		this.categories = categories;
	}

	
	
}
