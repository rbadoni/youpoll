package com.experts.services;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;

import com.experts.command.Question;
import com.experts.util.Logger;
import com.experts.util.Urls;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class SynchronizeDataItem
{
	private static final String TAG = "SynchronizeDataItem";

	public static void main(String a[]){
		
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();						
		
		SynchronizeDataItem data = new SynchronizeDataItem();
		String response = data.getData(nameValuePairs, Urls.FETCH_QUESTIONS);
		System.out.println(response);
	}

	public String getData(List<NameValuePair> pairs, String methodName)
	{
		String url = null;
		
		String serverResponse = null;
		BufferedReader in = null;

		HttpClient httpclient = new DefaultHttpClient();
		
		HttpParams params = httpclient.getParams();
		HttpConnectionParams.setConnectionTimeout(params, 2 * 1000 * 60);
		HttpConnectionParams.setSoTimeout(params, 2 * 1000 * 60);

		url = Urls.BASE_URL + methodName;

		HttpGet httpGet = new HttpGet(url);
		httpGet.addHeader("accept", "application/json");
		try
		{
			List<NameValuePair> nameValuePairs = pairs;

			/*if (nameValuePairs != null)
			{
				httpGet.setEntity(new UrlEncodedFormEntity(nameValuePairs, "UTF-8"));
			}*/

			HttpResponse response = httpclient.execute(httpGet);
			in = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
			StringBuffer sb = new StringBuffer("");
			String line = "";
			while ((line = in.readLine()) != null)
			{
				sb.append(line);
			}
			serverResponse = sb.toString();	
			httpclient.getConnectionManager().shutdown();
			return serverResponse;
		}
		catch (Exception ex)
		{
			Logger.d(this, TAG, "StackTrace : " + ex.getStackTrace().toString());
		}
		finally
		{
			try
			{
				if (in != null)
				{
					in.close();
				}
			}
			catch (IOException e)
			{
				Logger.d(this, TAG, "Exception StackTrace : " + e.getStackTrace().toString());
				throw new RuntimeException(e);
			}
		}
		Logger.d(getClass(), TAG, "Method Name : " + methodName + " serverResponse : " + serverResponse);		
		return serverResponse;
	}
}