package com.experts.util;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;

import com.experts.command.Question;
import com.experts.services.SynchronizeDataItem;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import android.content.AsyncTaskLoader;
import android.content.Context;

public class AsyncQuestionLoader extends AsyncTaskLoader<Question[]> {

	public AsyncQuestionLoader(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	@Override
	public Question[] loadInBackground() {
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

		SynchronizeDataItem data = new SynchronizeDataItem();
		String response = data.getData(nameValuePairs, Urls.FETCH_QUESTIONS);
		Gson gson = new GsonBuilder()
		   .setDateFormat(DateFormat.FULL, DateFormat.FULL).create();
		Question[] questions = gson.fromJson(response, Question[].class);
		System.out.println(response);
		return questions;
	}

}
