package com.experts.util;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;

import android.content.AsyncTaskLoader;
import android.content.Context;

import com.experts.command.Categories;
import com.experts.services.SynchronizeDataItem;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class AsyncCategoryLoader extends AsyncTaskLoader<Categories[]> {

	public AsyncCategoryLoader(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	@Override
	public Categories[] loadInBackground() {
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

		SynchronizeDataItem data = new SynchronizeDataItem();
		String response = data.getData(nameValuePairs, Urls.FETCH_CATEGORY);
		Gson gson = new GsonBuilder()
		   .setDateFormat(DateFormat.FULL, DateFormat.FULL).create();
		Categories[] categories = gson.fromJson(response, Categories[].class);
		System.out.println(response);
		return categories;
	}

}
