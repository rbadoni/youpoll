package com.experts.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.StringTokenizer;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;

public class FileUtils
{
	public static String externalStoragePath = Environment.getExternalStorageDirectory().getAbsolutePath();
	public static String SD_CARD_PATH = "data";
	private static String DEFAULT_APK_NAME = "Agent.apk";

	
	public static String getSmartAgentDirectoryPath()
	{
		return Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + "Android"
				+ File.separator + "data" + File.separator + "com.techie.agent" + File.separator;
	}

	public static String getSmartAgentLogsDirectoryPath()
	{
		return getSmartAgentDirectoryPath() + "logs" + File.separator;
	}
	
	public static String getDefaultApkLocation()
	{
		return getSmartAgentDirectoryPath() + DEFAULT_APK_NAME ;
	}
	
	public static void copyFiles(File sourceLocation, File targetLocation) throws IOException
	{
		if (sourceLocation.isDirectory())
		{
			if (!targetLocation.exists())
			{
				targetLocation.mkdir();
			}
			File[] files = sourceLocation.listFiles();
			for (File file : files)
			{
				InputStream in = new FileInputStream(file);
				OutputStream out = new FileOutputStream(targetLocation + "/" + file.getName());

				// Copy the bits from input stream to output stream
				byte[] buf = new byte[1024];
				int len;
				while ((len = in.read(buf)) > 0)
				{
					out.write(buf, 0, len);
				}
				in.close();
				out.close();
			}
		}
	}

	public static Bitmap decodeFile(File f, int IMAGE_MAX_SIZE)
	{
		Bitmap b = null;
		try
		{
			// Decode image size
			BitmapFactory.Options o = new BitmapFactory.Options();
			o.inJustDecodeBounds = true;
			BitmapFactory.decodeStream(new FileInputStream(f), null, o);
			int scale = 1;
			// IMAGE_MAX_SIZE is the maximum size you need to decide
			if (o.outHeight > IMAGE_MAX_SIZE || o.outWidth > IMAGE_MAX_SIZE)
			{
				scale = (int) Math.pow(2, (int) Math.round(Math.log(IMAGE_MAX_SIZE / (double) Math.max(o.outHeight, o.outWidth)) / Math.log(0.5)));
			}
			// Decode with inSampleSize
			BitmapFactory.Options o2 = new BitmapFactory.Options();
			o2.inSampleSize = scale;
			b = BitmapFactory.decodeStream(new FileInputStream(f), null, o2);

		}
		catch (FileNotFoundException e)
		{
			// handle exception here
		}
		return b;
	}

	public static Boolean movingImage(String srFile, String dtFile)
	{
		try
		{
			File f1 = new File(srFile);
			File f2 = new File(dtFile);
			InputStream in = new FileInputStream(f1);
			OutputStream out = new FileOutputStream(f2);

			byte[] buf = new byte[1024];
			int len;
			while ((len = in.read(buf)) > 0)
			{
				out.write(buf, 0, len);
			}
			in.close();
			out.close();
			return true;
		}
		catch (Exception ex)
		{
		//	Logger.d(FileUtils.class, "FileUtils", "getStackTrace : " + ex.getStackTrace().toString());
		}
		return false;
	}

	public static void createFileStructureInAndroid(String diretoryPath) // Input Like --> //
																			// com/ibolo/icrimeFighter/sanju.txt
	{
		String path = Environment.getExternalStorageDirectory().toString();
		StringTokenizer st = new StringTokenizer(diretoryPath, "/");
		int count = st.countTokens();

		for (int i = 0; i < count; i++)
		{
			path = path + File.separator + st.nextToken();
			if (count - 1 > i)
			{
				File f = new File(path);
				if (!f.isDirectory())
				{
					f.mkdir();
				}
			}
			else
			{
				File f2 = new File(path);
				try
				{
					f2.createNewFile();
				}
				catch (IOException e)
				{
					e.printStackTrace();
				}
			}
		}
	}

	public static String getPath(Uri uri, Context mContext)
	{
		String[] filePathColumn = { MediaStore.Images.Media.DATA };
		Cursor cursor = mContext.getContentResolver().query(uri, filePathColumn, null, null, null);
		cursor.moveToFirst();
		int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
		String galleryImagePath = cursor.getString(columnIndex);
		cursor.close();
		return galleryImagePath;
	}

	public static Bitmap getBitmapObj(String imagePath, Context mContext)
	{
		Bitmap bitmapObject = FileUtils.decodeFile(new File(imagePath), 100);
		Bitmap mBitmap = null;

		if (bitmapObject == null)
		{
			mBitmap = Bitmap.createBitmap(30, 30, Bitmap.Config.RGB_565);
			;
			Uri imgUri = Uri.parse(imagePath);
			OutputStream mFileOutStream;
			try
			{
				mFileOutStream = mContext.getContentResolver().openOutputStream(imgUri);
				boolean result = mBitmap.compress(Bitmap.CompressFormat.PNG, 90, mFileOutStream);
			//	Logger.v(FileUtils.class, "MatrixCAFApp", "bitmap result: " + result);

				mFileOutStream.flush();
				mFileOutStream.close();

			}
			catch (FileNotFoundException e)
			{
				e.printStackTrace();
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
		}
		else
		{
			return bitmapObject;
		}
		return mBitmap;
	}

	public static String getFileName(String filePath)
	{
		File file = new File(filePath);
		if (file.exists())
		{
			return file.getName();
		}
		return "";
	}

	public static Boolean checkFileSize(String galleryPath, String fileName)
	{
		File sdfile = Environment.getExternalStorageDirectory();

		File _photo = new File(sdfile.getAbsolutePath() + File.separator + "Android" + File.separator + "data" + File.separator + "com.pinelabs.matrixcafapp" + File.separator + ".files" + File.separator + fileName);
		if (!_photo.exists())
		{
			_photo.getParentFile().mkdirs();
			try
			{
				_photo.createNewFile();
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
		}

		Boolean success = FileUtils.movingImage(galleryPath, _photo.getAbsolutePath());

		return success;
	}
}