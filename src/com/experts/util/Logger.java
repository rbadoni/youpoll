package com.experts.util;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.util.Log;

public class Logger
{
	static File f = null;
	static FileWriter fw = null;
	static SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
	static Map<Integer, String> loglevels = new HashMap<Integer, String>();
	static int currentLogLevel = Log.VERBOSE;
	static List<String> buffer = new ArrayList<String>();
	static Object obj = new Object();
	static final String filePath = FileUtils.getSmartAgentLogsDirectoryPath();
	static final int MAXFILESIZE = 10;
	static final int MAXFILES = 3;
	static final String ARCHIVE_PATH = FileUtils.getSmartAgentDirectoryPath();
	static final String ZipFilePath = FileUtils.getSmartAgentLogsDirectoryPath();
	static String zipFileName = "matrixZipFile";

	static
	{
		loglevels.put(Log.DEBUG, "DEBUG");
		loglevels.put(Log.ASSERT, "ASSERT");
		loglevels.put(Log.ERROR, "ERROR");
		loglevels.put(Log.INFO, "INFO");
		loglevels.put(Log.VERBOSE, "VERBOSE");
		loglevels.put(Log.WARN, "WARN");

		Log.d("Agent", "creating directory");
		new File(filePath).mkdirs();
		createNewFileWriter();
	}

	public static void v(Object obj, String tag, String message)
	{
		logMessage(obj.getClass(), Log.VERBOSE, tag, message);
	}

	public static void e(Object obj, String tag, String message)
	{
		logMessage(obj.getClass(), Log.ERROR, tag, message);
	}

	public static void e(Object obj, String tag, String message, Exception e)
	{
		logMessage(obj.getClass(), Log.ERROR, tag, message);
		logMessage(obj.getClass(), Log.ERROR, tag, e.getLocalizedMessage() + " : " + e.getMessage());
		if (e.getCause() != null)
			logMessage(obj.getClass(), Log.ERROR, tag, e.getCause().toString());
		StackTraceElement s[] = e.getStackTrace();
		if (s != null)
		{
			for (StackTraceElement se : s)
			{
				logMessage(obj.getClass(), Log.ERROR, tag, se.getClassName() + ":" + se.getFileName() + ":" + se.getMethodName() + ":" + se.getLineNumber() + ":" + se.toString());
			}
		}
	}

	public static void e(Object obj, String tag, String message, Error e)
	{
		logMessage(obj.getClass(), Log.ERROR, tag, message);
		logMessage(obj.getClass(), Log.ERROR, tag, e.getLocalizedMessage() + " : " + e.getMessage());
		if (e.getCause() != null)
			logMessage(obj.getClass(), Log.ERROR, tag, e.getCause().toString());
		StackTraceElement s[] = e.getStackTrace();
		if (s != null)
		{
			for (StackTraceElement se : s)
			{
				logMessage(obj.getClass(), Log.ERROR, tag, se.getClassName() + ":" + se.getFileName() + ":" + se.getMethodName() + ":" + se.getLineNumber() + ":" + se.toString());
			}
		}
	}

	public static void w(Object obj, String tag, String message)
	{
		logMessage(obj.getClass(), Log.WARN, tag, message);
	}

	public static void i(Object obj, String tag, String message)
	{
		logMessage(obj.getClass(), Log.INFO, tag, message);
	}

	public static void a(Object obj, String tag, String message)
	{
		logMessage(obj.getClass(), Log.ASSERT, tag, message);
	}

	public static void d(Class c, String tag, String message)
	{
		logMessage(c, Log.DEBUG, tag, message);
	}

	public static void d(Object obj, String tag, String message)
	{
		logMessage(obj.getClass(), Log.DEBUG, tag, message);
	}

	private static void createNewFileWriter()
	{
		new File(filePath).mkdirs();
		File folder = new File(filePath);
		File files[] = folder.listFiles();
		File latestFile = null;
		long maxTime = 0;
		for (File f1 : files)
		{
			if (maxTime < f1.lastModified())
			{
				latestFile = f1;
				maxTime = f1.lastModified();
			}
		}
		if (latestFile == null)
		{
			latestFile = new File(filePath + "log0.log");
		}
		int maxCount = getFileCount(latestFile.getName());

		int deleteCount = -1;
		if (maxCount > MAXFILES)
			deleteCount = maxCount - MAXFILES;

		for (File f1 : files)
		{
			int count = getFileCount(f1.getName());
			if (count <= deleteCount)
			{
				f1.delete();
			}
		}

		f = latestFile;
		if (f.exists())
		{
			if (sizeExceeded(f))
			{
				f = null;
				f = new File(filePath + "log" + (1 + maxCount) + ".log");
			}
		}
		try
		{
			// f.createNewFile();
			fw = new FileWriter(f, true);
		}
		catch (IOException e)
		{
			System.out.println("LOGGER: Unable to open log file: " + e.getMessage());
		}
	}

	private static int getFileCount(String fileName)
	{
		int maxCount = 0;
		String c = fileName.substring(3, fileName.length() - 4);
		if (StringUtils.isNotNull(c))
		{
			try
			{
				maxCount = Integer.parseInt(c);
			}
			catch (Exception e)
			{
			}
		}
		return maxCount;
	}

	private static boolean sizeExceeded(File f2)
	{
		long filesize = f.length();
		long filesizeInMB = filesize / (1024 * 1024);
		return filesizeInMB >= MAXFILESIZE;
	}

	private static void logMessage(Class c, int logLevel, String tag, String message)
	{
		if (logLevel < currentLogLevel)
			return;

		synchronized (obj)
		{
			buffer.add(formatter.format(new Date()) + " : " + c.getCanonicalName() + " : " + loglevels.get(logLevel) + " : " + tag + " : " + message);

			if (buffer.size() <= 5)
			{
				return;
			}

			writeBuffer();
			if (sizeExceeded(f))
			{
				close();
				createNewFileWriter();
			}
		}
	}

	private static void writeBuffer()
	{
		try
		{
			for (String s : buffer)
			{
				Log.d("Agent LOGS", s);
				fw.write(s);
				fw.write("\n");
			}
			fw.flush();
		}
		catch (IOException e)
		{
			Logger.e(Logger.class, "ERROR", "Unable to write to Log file: ", e);
		}
		buffer.clear();
	}

	public static void close()
	{
		try
		{
			writeBuffer();
			fw.close();
		}
		catch (IOException e)
		{
		}
	}
}